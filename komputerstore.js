

// Elemements from index.html
const laptopsElement = document.getElementById("laptops");
const loanButtonElement = document.getElementById("get_loan_btn");
const workButtonElement = document.getElementById("work_btn");
const bankButtonElement = document.getElementById("bank_btn");
const buyComputerBtnElement = document.getElementById("buy_computer_btn");
const workPayElement = document.getElementById("WorkPay");
const laptopFeatElement = document.getElementById("LaptopFeat");
const laptopPriceElement = document.getElementById("LaptopPrice");
const laptopSpecsElement = document.getElementById("LaptopSpecs");
const laptopStockElement = document.getElementById("LaptopStock");
const laptopActiveElement = document.getElementById("LaptopActive");
const bankBalanceElement = document.getElementById("bankBalance");
const quantityElement = document.getElementById("quantity");
const loanPaymentElement = document.getElementById("loan_payment");
const laptopImageElement = document.getElementById("laptop_image");

// Variables
let laptops = [];
let totalLoan = 0.0;
let pay = 0.0;
let bankMoney = 200;
let tenPercent = 0;
let laptopPrice = 0;
let urlImage = "https://noroff-komputer-store-api.herokuapp.com/";

//API Fetch
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

// Adds laptops
const addLaptopsToMenu = (laptops) => {
    laptops.forEach(laptop => addLaptopToMenu(laptop));
    laptopFeatElement.innerText = laptops[0].LaptopFeat;
}
// Adds title
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);

    bankBalanceElement.innerText = bankMoney;
}
// Handles selection of laptop
const handleLaptopChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    laptopFeatElement.innerText = selectedLaptop.description;
    laptopPriceElement.innerText = selectedLaptop.price;
    laptopSpecsElement.innerText = selectedLaptop.specs;
    laptopStockElement.innerText = selectedLaptop.stock;
    laptopActiveElement.innerText = selectedLaptop.active;

    const imageUrl = selectedLaptop.image;
    laptopImageElement.src = urlImage + imageUrl;

    laptopPrice = selectedLaptop.price;
}
// Handles buying a laptop and deducts money
const handleBuyLaptop = () => {
    if (laptopPrice <= bankMoney) {
        bankMoney = bankMoney - laptopPrice;
        bankBalanceElement.innerText = bankMoney;
        alert("Congratulations, you are now the owner of a new laptop!");
    }  else {
        alert("Sorry, you can not afford this laptop.");
    }
}
// Handles everything about the loan. 
// Attempts to issue a loan, repays the loan from bank account.
// Adjusts amount of money available to loan
const handleLoan = () => {
    const takeLoan = prompt("Enter amount you want to lend:")

    if (takeLoan <= bankMoney*2 && totalLoan == 0)  {
        const loan = parseFloat(takeLoan)
        loanPaymentElement.innerText = "Loan:" + loan;
        totalLoan = loan;
    }
    else if (takeLoan > bankMoney*2 && totalLoan == 0){
        const smallerLoan = prompt("You can not lend that much. Please enter a smaller amount.");
        const smallLoan = parseFloat(smallerLoan);
        totalLoan = smallLoan;
        loanPaymentElement.innerText = "Loan:" + smallLoan;
    }
    else {
        const payLoan = prompt("Please pay your loan before taking a new one:");
        const loanPayment = parseFloat(payLoan);
        totalLoan = totalLoan - loanPayment
        loanPaymentElement.innerText = "Loan:" + totalLoan;
        bankMoney = bankMoney - loanPayment;
    }
    bankBalanceElement.innerText = bankMoney;
}

// Bank button, adds money to bank and loan
const handleBank = () => {
    bankBalanceElement.innerText = bankMoney;
    if (totalLoan != 0) {
        tenPercent = pay*0.1;
        totalLoan = totalLoan - tenPercent;
        pay = pay - tenPercent;
        bankMoney = bankMoney + pay;
    }
    else {
        bankMoney = pay + bankMoney;
    }
    pay = 0;
    workPayElement.innerText = pay;
    bankBalanceElement.innerText = bankMoney + pay;
    loanPaymentElement.innerText = "Loan - " + totalLoan;
}

// Work button, adds a salary for each click.
const handleWork = () => {
    pay = pay + 100;
    workPayElement.innerText = pay;
}

// Handles events
laptopsElement.addEventListener("change", handleLaptopChange);
buyComputerBtnElement.addEventListener("click", handleBuyLaptop);
loanButtonElement.addEventListener("click", handleLoan);
workButtonElement.addEventListener("click", handleWork);
bankButtonElement.addEventListener("click", handleBank);
